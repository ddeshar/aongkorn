<?php
/**
 * -----------------------------------------------------------------------------
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 * -----------------------------------------------------------------------------
 */

namespace frontend\assets;

use yii\web\AssetBundle;
use Yii;

// set @themes alias so we do not have to update baseUrl every time we change themes
Yii::setAlias('@themes', Yii::$app->view->theme->baseUrl);

/**
 * -----------------------------------------------------------------------------
 * @author Qiang Xue <qiang.xue@gmail.com>
 *
 * @since 2.0
 * -----------------------------------------------------------------------------
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@themes';

    public $css = [
      'css/nav.css',
      'css/bootstrap.css',
      'css/font-awesome.min.css',
      'css/slicknav.css',
      'css/style.css',
      'css/animate.css',
      //'css/responsive.css',


      'css/colors/red.css',
      'css/colors/jade.css',
      'css/colors/green.css',
      'css/colors/blue.css',
      'css/colors/beige.css',
      'css/colors/cyan.css',
      'css/colors/orange.css',
      'css/colors/peach.css',
      'css/colors/pink.css',
      'css/colors/purple.css',
      'css/colors/sky-blue.css',
      'css/colors/yellow.css',
    ];
    public $js = [

      'js/jquery-2.1.4.min.js',
      'js/jquery.migrate.js',
      'js/modernizrr.js',
      'js/bootstrap.min.js',
      'js/jquery.fitvids.js',
      'js/owl.carousel.min.js',
      'js/nivo-lightbox.min.js',
      'js/jquery.isotope.min.js',
      'js/jquery.appear.js',
      'js/count-to.js',
      'js/jquery.textillate.js',
      'js/jquery.lettering.js',
      'js/jquery.easypiechart.min.js',
      'js/jquery.nicescroll.min.js',
      'js/jquery.parallax.js',
      'js/mediaelement-and-player.js',
      'js/jquery.slicknav.js',
      'js/script.js',
      '//maps.googleapis.com/maps/api/js?sensor=false',
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}

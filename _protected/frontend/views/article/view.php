<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- Start Content -->
  <div class="container">
    <div class="row blog-post-page">
      <div class="col-md-9 blog-box">

        <!-- Start Single Post Area -->
        <div class="blog-post gallery-post">

          <!-- Start Single Post (Gallery Slider) -->
          <div class="post-head">
            <div class="touch-slider post-slider">
              <div class="item">
                <a class="lightbox" title="This is an image title" href="../themes/newa/images/blog-02.jpg" data-lightbox-gallery="gallery1">
                  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                  <img alt="" src="../themes/newa/images/blog-02.jpg">
                </a>
              </div>
              <div class="item">
                <a class="lightbox" title="This is an image title" href="../themes/newa/images/blog-03.jpg" data-lightbox-gallery="gallery1">
                  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                  <img alt="" src="../themes/newa/images/blog-03.jpg">
                </a>
              </div>
              <div class="item">
                <a class="lightbox" title="This is an image title" href="../themes/newa/images/blog-04.jpg" data-lightbox-gallery="gallery1">
                  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                  <img alt="" src="../themes/newa/images/blog-04.jpg">
                </a>
              </div>
            </div>
          </div>
          <!-- End Single Post (Gallery) -->

          <!-- Start Single Post Content -->
          <div class="post-content">
            <div class="post-type"><i class="fa fa-picture-o"></i></div>
            <h2>ทำไมจึงต้องนำมาใช้?</h2>
            <ul class="post-meta">
              <li>โดย <a href="#">พระมหาดีเป็นดร้า</a></li>
              <li>December 30, 2013</li>
              <li><a href="#">ฝ่ายวิชาการ</a>, <a href="#">ฝ่ายนิสิตสัมพันธ</a></li>
            </ul>
            <p>หลักฐานที่เป็นข้อเท็จจริงยืนยันมานานแล้ว ว่าเนื้อหาที่อ่านรู้เรื่องนั้นจะไปกวนสมาธิของคนอ่านให้เขวไปจากส่วนที้เป็น Layout เรานำ Lorem Ipsum มาใช้เพราะความที่มันมีการกระจายของตัวอักษรธรรมดาๆ แบบพอประมาณ ซึ่งเอามาใช้แทนการเขียนว่า ‘ตรงนี้เป็นเนื้อหา, ตรงนี้เป็นเนื้อหา' ได้ และยังทำให้มองดูเหมือนกับภาษาอังกฤษที่อ่านได้ปกติ ปัจจุบันมีแพ็กเกจของซอฟท์แวร์การทำสื่อสิ่งพิมพ์ และซอฟท์แวร์การสร้างเว็บเพจ (Web Page Editor) หลายตัวที่ใช้ Lorem Ipsum เป็นแบบจำลองเนื้อหาที่เป็นค่าตั้งต้น และเวลาที่เสิร์ชด้วยคำว่า 'lorem ipsum' ผลการเสิร์ชที่ได้ก็จะไม่พบบรรดาเว็บไซต์ที่ยังคงอยู่ในช่วงเริ่มสร้างด้วย โดยหลายปีที่ผ่านมาก็มีการคิดค้นเวอร์ชั่นต่างๆ ของ Lorem Ipsum ขึ้นมาใช้ บ้างก็เป็นความบังเอิญ บ้างก็เป็นความตั้งใจ (เช่น การแอบแทรกมุกตลก)</p>
            <p>หลักฐานที่เป็นข้อเท็จจริงยืนยันมานานแล้ว ว่าเนื้อหาที่อ่านรู้เรื่องนั้นจะไปกวนสมาธิของคนอ่านให้เขวไปจากส่วนที้เป็น Layout เรานำ Lorem Ipsum มาใช้เพราะความที่มันมีการกระจายของตัวอักษรธรรมดาๆ แบบพอประมาณ ซึ่งเอามาใช้แทนการเขียนว่า ‘ตรงนี้เป็นเนื้อหา, ตรงนี้เป็นเนื้อหา' ได้ และยังทำให้มองดูเหมือนกับภาษาอังกฤษที่อ่านได้ปกติ ปัจจุบันมีแพ็กเกจของซอฟท์แวร์การทำสื่อสิ่งพิมพ์ และซอฟท์แวร์การสร้างเว็บเพจ (Web Page Editor) หลายตัวที่ใช้ Lorem Ipsum เป็นแบบจำลองเนื้อหาที่เป็นค่าตั้งต้น และเวลาที่เสิร์ชด้วยคำว่า 'lorem ipsum' ผลการเสิร์ชที่ได้ก็จะไม่พบบรรดาเว็บไซต์ที่ยังคงอยู่ในช่วงเริ่มสร้างด้วย โดยหลายปีที่ผ่านมาก็มีการคิดค้นเวอร์ชั่นต่างๆ ของ Lorem Ipsum ขึ้นมาใช้ บ้างก็เป็นความบังเอิญ บ้างก็เป็นความตั้งใจ (เช่น การแอบแทรกมุกตลก)</p>
            <p>หลักฐานที่เป็นข้อเท็จจริงยืนยันมานานแล้ว ว่าเนื้อหาที่อ่านรู้เรื่องนั้นจะไปกวนสมาธิของคนอ่านให้เขวไปจากส่วนที้เป็น Layout เรานำ Lorem Ipsum มาใช้เพราะความที่มันมีการกระจายของตัวอักษรธรรมดาๆ แบบพอประมาณ ซึ่งเอามาใช้แทนการเขียนว่า ‘ตรงนี้เป็นเนื้อหา, ตรงนี้เป็นเนื้อหา' ได้ และยังทำให้มองดูเหมือนกับภาษาอังกฤษที่อ่านได้ปกติ ปัจจุบันมีแพ็กเกจของซอฟท์แวร์การทำสื่อสิ่งพิมพ์ และซอฟท์แวร์การสร้างเว็บเพจ (Web Page Editor) หลายตัวที่ใช้ Lorem Ipsum เป็นแบบจำลองเนื้อหาที่เป็นค่าตั้งต้น และเวลาที่เสิร์ชด้วยคำว่า 'lorem ipsum' ผลการเสิร์ชที่ได้ก็จะไม่พบบรรดาเว็บไซต์ที่ยังคงอยู่ในช่วงเริ่มสร้างด้วย โดยหลายปีที่ผ่านมาก็มีการคิดค้นเวอร์ชั่นต่างๆ ของ Lorem Ipsum ขึ้นมาใช้ บ้างก็เป็นความบังเอิญ บ้างก็เป็นความตั้งใจ (เช่น การแอบแทรกมุกตลก)</p>
            <p>หลักฐานที่เป็นข้อเท็จจริงยืนยันมานานแล้ว ว่าเนื้อหาที่อ่านรู้เรื่องนั้นจะไปกวนสมาธิของคนอ่านให้เขวไปจากส่วนที้เป็น Layout เรานำ Lorem Ipsum มาใช้เพราะความที่มันมีการกระจายของตัวอักษรธรรมดาๆ แบบพอประมาณ ซึ่งเอามาใช้แทนการเขียนว่า ‘ตรงนี้เป็นเนื้อหา, ตรงนี้เป็นเนื้อหา' ได้ และยังทำให้มองดูเหมือนกับภาษาอังกฤษที่อ่านได้ปกติ ปัจจุบันมีแพ็กเกจของซอฟท์แวร์การทำสื่อสิ่งพิมพ์ และซอฟท์แวร์การสร้างเว็บเพจ (Web Page Editor) หลายตัวที่ใช้ Lorem Ipsum เป็นแบบจำลองเนื้อหาที่เป็นค่าตั้งต้น และเวลาที่เสิร์ชด้วยคำว่า 'lorem ipsum' ผลการเสิร์ชที่ได้ก็จะไม่พบบรรดาเว็บไซต์ที่ยังคงอยู่ในช่วงเริ่มสร้างด้วย โดยหลายปีที่ผ่านมาก็มีการคิดค้นเวอร์ชั่นต่างๆ ของ Lorem Ipsum ขึ้นมาใช้ บ้างก็เป็นความบังเอิญ บ้างก็เป็นความตั้งใจ (เช่น การแอบแทรกมุกตลก)</p>
            <p>หลักฐานที่เป็นข้อเท็จจริงยืนยันมานานแล้ว ว่าเนื้อหาที่อ่านรู้เรื่องนั้นจะไปกวนสมาธิของคนอ่านให้เขวไปจากส่วนที้เป็น Layout เรานำ Lorem Ipsum มาใช้เพราะความที่มันมีการกระจายของตัวอักษรธรรมดาๆ แบบพอประมาณ ซึ่งเอามาใช้แทนการเขียนว่า ‘ตรงนี้เป็นเนื้อหา, ตรงนี้เป็นเนื้อหา' ได้ และยังทำให้มองดูเหมือนกับภาษาอังกฤษที่อ่านได้ปกติ ปัจจุบันมีแพ็กเกจของซอฟท์แวร์การทำสื่อสิ่งพิมพ์ และซอฟท์แวร์การสร้างเว็บเพจ (Web Page Editor) หลายตัวที่ใช้ Lorem Ipsum เป็นแบบจำลองเนื้อหาที่เป็นค่าตั้งต้น และเวลาที่เสิร์ชด้วยคำว่า 'lorem ipsum' ผลการเสิร์ชที่ได้ก็จะไม่พบบรรดาเว็บไซต์ที่ยังคงอยู่ในช่วงเริ่มสร้างด้วย โดยหลายปีที่ผ่านมาก็มีการคิดค้นเวอร์ชั่นต่างๆ ของ Lorem Ipsum ขึ้นมาใช้ บ้างก็เป็นความบังเอิญ บ้างก็เป็นความตั้งใจ (เช่น การแอบแทรกมุกตลก)</p>

            <div class="post-bottom clearfix">
              <div class="post-tags-list">
                <a href="#">ฝ่ายวิชาการ</a>
                <a href="#">ฝ่ายนิสิตสัมพันธ</a>
                <a href="#">ฝ่ายกิจกรรม</a>

              </div>
              <div class="post-share">
                <span>แบ่งปันโพสต์นี้:</span>
                <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                <a class="gplus" href="#"><i class="fa fa-google-plus"></i></a>
                <a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                <a class="mail" href="#"><i class="fa fa-envelope"></i></a>
              </div>
            </div>
            <div class="author-info clearfix">
              <div class="author-image">
                <a href="#"><img alt="" src="../themes/newa/images/author.png" /></a>
              </div>
              <div class="author-bio">
                <h4>มันมีที่มาอย่างไร?</h4>
                <p>ตรงกันข้ามกับความเชื่อที่นิยมกัน Lorem Ipsum ไม่ได้เป็นเพียงแค่ชุดตัวอักษรที่สุ่มขึ้นมามั่วๆ แต่หากมีที่มาจากวรรณกรรมละตินคลาสสิกชิ้นหนึ่งในยุค 45 ปีก่อนคริสตศักราช ทำให้มันมีอายุถึงกว่า 2000 ปีเลยทีเดียว ริชาร์ด แมคคลินท็อค ศาสตราจารย์ชาวละติน จากวิทยาลัยแฮมพ์เด็น-ซิดนีย์ ในรัฐเวอร์จิเนียร์ นำคำภาษาละตินคำว่า</p>
              </div>
            </div>
          </div>
          <!-- End Single Post Content -->

        </div>
        <!-- End Single Post Area -->

      </div>

      <!-- Sidebar -->
      <div class="col-md-3 sidebar right-sidebar">

        <!-- Categories Widget -->
        <div class="widget widget-categories">
          <h4>งานฝ่ายตางๆ <span class="head-line"></span></h4>
          <ul>
            <li><a href="#">ฝ่าย วิชาการ</a></li>
            <li><a href="#">ฝ่าย นิสิตสัมพันธ์</a></li>
            <li><a href="#">ฝ่าย กิจกรรม</a></li>
            <li><a href="#">ฝ่าย ประชาสัมพันธ์และเผยแผ่</a></li>
            <li><a href="#">เลขานุการ</a></li>
            <li><a href="#">ฝ่าย ปฏิคม</a></li>
            <li><a href="#">ฝ่าย การเงิน</a></li>
          </ul>
        </div>

        <!-- Popular Posts widget -->
        <div class="widget widget-popular-posts">
          <h4>บทความ แนะนำ<span class="head-line"></span></h4>
          <ul>
            <li>
              <div class="widget-thumb">
                <a href="#"><img src="../themes/newa/images/blog-mini-01.jpg" alt="" /></a>
              </div>
              <div class="widget-content">
                <h5><a href="#">จะนำมาใช้ได้จากที่ไหน</a></h5>
                <span>Jul 29 2013</span>
              </div>
              <div class="clearfix"></div>
            </li>
            <li>
              <div class="widget-thumb">
                <a href="#"><img src="../themes/newa/images/blog-mini-02.jpg" alt="" /></a>
              </div>
              <div class="widget-content">
                <h5><a href="#">จะนำมาใช้ได้จากที่ไหน</a></h5>
                <span>Jul 29 2013</span>
              </div>
              <div class="clearfix"></div>
            </li>
            <li>
              <div class="widget-thumb">
                <a href="#"><img src="../themes/newa/images/blog-mini-03.jpg" alt="" /></a>
              </div>
              <div class="widget-content">
                <h5><a href="#">จะนำมาใช้ได้จากที่ไหน</a></h5>
                <span>Jul 29 2013</span>
              </div>
              <div class="clearfix"></div>
            </li>
          </ul>
        </div>

        <!-- Video Widget -->
        <div class="widget">
          <h4>วีดีโอ แนะนำ <span class="head-line"></span></h4>
          <div>
            <iframe src="http://player.vimeo.com/video/63322694?byline=0&amp;portrait=0&amp;badge=0" width="800" height="450"></iframe>
          </div>
        </div>

        <!-- Tags Widget -->
        <div class="widget widget-tags">
          <h4>Tags <span class="head-line"></span></h4>
          <div class="tagcloud">
            <a href="#">ฝ่ายวิชาการ</a>
            <a href="#">ฝ่ายนิสิตสัมพันธ</a>
            <a href="#">ฝ่ายกิจกรรม</a>
            <a href="#">ฝ่ายประชาสัมพันธ์และเผยแผ</a>
            <a href="#">เลขานุการ</a>
            <a href="#">ฝ่ายปฏิคม</a>
            <a href="#">ฝ่ายการเงิน</a>
          </div>
        </div>

      </div>
      <!--End sidebar-->


    </div>

  </div>
<!-- End content -->

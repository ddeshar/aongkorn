<?php
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <!-- Full Body Container -->
    <div id="container">

      <!-- Start Header Section -->
      <div class="hidden-header"></div>
      <header class="clearfix">

        <!-- Start Top Bar -->
        <div class="top-bar">
          <div class="container">
            <div class="row">
              <div class="col-md-7">
                <!-- Start Contact Info -->
                <ul class="contact-details">
                  <li><a href="#"><i class="fa fa-map-marker"></i> 158 ถนนศรีโสธร ต.หน้าเมือง อ.เมือง จ.ฉะเชิงเทรา 24000</a></li>
                  <li><a href="#"><i class="fa fa-envelope-o"></i> jedeshar@gmail.com</a></li>
                  <li><a href="#"><i class="fa fa-phone"></i> +66 984 577 342</a></li>
                </ul>
                <!-- End Contact Info -->
              </div>
              <!-- .col-md-6 -->
              <div class="col-md-5">
                <!-- Start Social Links -->
                <ul class="social-list">
                  <li><a class="facebook itl-tooltip" data-placement="bottom" title="Facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a class="twitter itl-tooltip" data-placement="bottom" title="Twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                  <li><a class="google itl-tooltip" data-placement="bottom" title="Google Plus" href="#"><i class="fa fa-google-plus"></i></a></li>
                  <li><a class="instgram itl-tooltip" data-placement="bottom" title="Instagram" href="#"><i class="fa fa-instagram"></i></a></li>
                </ul>
                <!-- End Social Links -->
              </div>
              <!-- .col-md-6 -->
            </div>
            <!-- .row -->
          </div>
          <!-- .container -->
        </div>
        <!-- .top-bar -->
        <!-- End Top Bar -->

        <!-- Start  Logo & Naviagtion  -->
        <?php

            NavBar::begin([
                'brandLabel' => Yii::t('app', Yii::$app->name),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar navbar-default navbar-top ',
                ],
            ]);

            // everyone can see Home page
            $menuItems[] = ['label' => Yii::t('app', 'หน้าแรก'), 'url' => ['/site/index']];
            $menuItems[] = ['label' => Yii::t('app', 'เกียวกับเรา'), 'url' => ['/site/about']];
            $menuItems[] = ['label' => Yii::t('app', 'กิจกรรม'), 'url' => ['/article/index']];
            $menuItems[] = ['label' => Yii::t('app', 'ดาวน์โหลด'), 'url' => ['/site/signup']];
            // $menuItems[] = ['label' => Yii::t('app', 'ติดต่อเรา'), 'url' => ['/site/contact']];

            $menuItems[] = [
            'label' => Yii::t('app', 'Contact'),
            'url' => ['#'],
                'items' => [
                    ['label' => Yii::t('app', '1'), 'url' => ['/article/index']],
                    ['label' => Yii::t('app', '2'), 'url' => ['/article/index']],
                    ['label' => Yii::t('app', '3'), 'url' => ['/article/index']],
                ]
            ];

            // we do not need to display Article/index, About and Contact pages to editor+ roles
            // if (!Yii::$app->user->can('editor')){
            //     $menuItems[] = ['label' => Yii::t('app', 'บทความ'), 'url' => ['/article/index']];
            //     $menuItems[] = ['label' => Yii::t('app', 'เกียวกับเรา'), 'url' => ['/site/about']];
            //     $menuItems[] = ['label' => Yii::t('app', 'ติดต่อ'), 'url' => ['/site/contact']];
            // }
            // // display Article admin page to editor+ roles
            // if (Yii::$app->user->can('editor')){
            //     $menuItems[] = ['label' => Yii::t('app', 'บทความ s'), 'url' => ['/article/admin']];
            // }
            // // display Signup and Login pages to guests of the site
            // if (Yii::$app->user->isGuest){
            //     $menuItems[] = ['label' => Yii::t('app', 'Signup'), 'url' => ['/site/signup']];
            //     $menuItems[] = ['label' => Yii::t('app', 'Login'), 'url' => ['/site/login']];
            // }
            // // display Logout to all logged in users
            // else{
            //     $menuItems[] = [
            //         'label' => Yii::t('app', 'Logout'). ' (' . Yii::$app->user->identity->username . ')',
            //         'url' => ['/site/logout'],
            //         'linkOptions' => ['data-method' => 'post']
            //     ];
            // }


            echo Nav::widget([
                'options' => ['class' => 'nav navbar-nav navbar-right dropdown'],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?>
        <!-- End Header Logo & Naviagtion -->

      </header>
      <!-- End Header Section -->

      <div class="container">
        <?= Breadcrumbs::widget([
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
      </div>

      <?= $content ?>
    </div>


    <!-- Start Footer Section -->
    <footer>
      <div class="container">
        <div class="row footer-widgets">

          <!-- Start Subscribe & Social Links Widget -->
          <div class="col-md-3 col-xs-12">
            <div class="footer-widget mail-subscribe-widget">
              <h4>แจ้งเตือน<span class="head-line"></span></h4>
              <p>เข้าร่วมรายการจดหมายของเราที่จะที่อัปเดตและได้รับการบอกกล่าวเกี่ยวกับที่ออกใหม่ของเรา!</p>
              <form class="subscribe">
                <input type="text" placeholder="mail@example.com">
                <input type="submit" class="btn-system" value="Send">
              </form>
            </div>
            <div class="footer-widget social-widget">
              <h4>ติดตามเรา<span class="head-line"></span></h4>
              <ul class="social-icons">
                <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a class="google" href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a class="instgram" href="#"><i class="fa fa-instagram"></i></a></li>
              </ul>
            </div>
          </div>
          <!-- .col-md-3 -->
          <!-- End Subscribe & Social Links Widget -->


          <!-- Start Twitter Widget -->
          <div class="col-md-3 col-xs-12">
            <div class="footer-widget twitter-widget">
              <h4>Twitter Feed<span class="head-line"></span></h4>
              <ul>
                <li>
                  <p><a href="#">@GrayGrids </a> Lorem ipsum dolor et, consectetur adipiscing eli.</p>
                  <span>28 February 2014</span>
                </li>
                <li>
                  <p><a href="#">@GrayGrids </a> Lorem ipsum dolor et, consectetur adipiscing eli.An Fusce eleifend aliquet nis application.</p>
                  <span>26 February 2014</span>
                </li>
                <li>
                  <p><a href="#">@GrayGrids </a> Lorem ipsum dolor et, consectetur adipiscing eli.</p>
                  <span>28 February 2014</span>
                </li>
              </ul>
            </div>
          </div>
          <!-- .col-md-3 -->
          <!-- End Twitter Widget -->


          <!-- Start Flickr Widget -->
          <div class="col-md-3 col-xs-12">
            <div class="footer-widget flickr-widget">
              <h4>Flicker Feed<span class="head-line"></span></h4>
              <ul class="flickr-list">
                <li>
                  <a href="/themes/newa/images/flickr-01.jpg" class="lightbox">
                    <img alt="" src="/themes/newa/images/flickr-01.jpg">
                  </a>
                </li>
                <li>
                  <a href="/themes/newa/images/flickr-02.jpg" class="lightbox">
                    <img alt="" src="/themes/newa/images/flickr-02.jpg">
                  </a>
                </li>
                <li>
                  <a href="/themes/newa/images/flickr-03.jpg" class="lightbox">
                    <img alt="" src="/themes/newa/images/flickr-03.jpg">
                  </a>
                </li>
                <li>
                  <a href="/themes/newa/images/flickr-04.jpg" class="lightbox">
                    <img alt="" src="/themes/newa/images/flickr-04.jpg">
                  </a>
                </li>
                <li>
                  <a href="/themes/newa/images/flickr-05.jpg" class="lightbox">
                    <img alt="" src="/themes/newa/images/flickr-05.jpg">
                  </a>
                </li>
                <li>
                  <a href="/themes/newa/images/flickr-06.jpg" class="lightbox">
                    <img alt="" src="/themes/newa/images/flickr-06.jpg">
                  </a>
                </li>
                <li>
                  <a href="/themes/newa/images/flickr-07.jpg" class="lightbox">
                    <img alt="" src="/themes/newa/images/flickr-07.jpg">
                  </a>
                </li>
                <li>
                  <a href="/themes/newa/images/flickr-08.jpg" class="lightbox">
                    <img alt="" src="/themes/newa/images/flickr-08.jpg">
                  </a>
                </li>
                <li>
                  <a href="/themes/newa/images/flickr-09.jpg" class="lightbox">
                    <img alt="" src="/themes/newa/images/flickr-09.jpg">
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <!-- .col-md-3 -->
          <!-- End Flickr Widget -->


          <!-- Start Contact Widget -->
          <div class="col-md-3 col-xs-12">
            <div class="footer-widget contact-widget">
              <h4><img src="images/footer-margo.png" class="img-responsive" alt="Footer Logo" /></h4>
              <p>องค์กรบริหารนิสิต วิทยาลัยสงฆ์พุทธโสธร 158 ถนนศรีโสธร ต.หน้าเมือง อ.เมือง จ.ฉะเชิงเทรา 24000</p>
              <ul>
                <li><span>Helpline Numbers (8AM to 10PM)</span> +66 984 577 342</li>
                <li><span>Email:</span> jedeshar@gmail.com</li>
                <li><span>Website:</span> www.ddeshar.com.np</li>
              </ul>
            </div>
          </div>
          <!-- .col-md-3 -->
          <!-- End Contact Widget -->


        </div>
        <!-- .row -->

        <!-- Start Copyright -->
        <div class="copyright-section">
          <div class="row">
            <div class="col-md-6">
              <p>&copy; <?= Yii::t('app', Yii::$app->name) ?> <?= date('Y') ?> - All Rights Reserved <a href="http://ddeshar.com.np">Z - Team</a> </p>
            </div>
            <!-- .col-md-6 -->
            <div class="col-md-6">
              <ul class="footer-nav">
                <li><a href="#">Sitemap</a>
                </li>
                <li><a href="#">Privacy Policy</a>
                </li>
                <li><a href="#">Contact</a>
                </li>
              </ul>
            </div>
            <!-- .col-md-6 -->
          </div>
          <!-- .row -->
        </div>
        <!-- End Copyright -->

      </div>
    </footer>
    <!-- End Footer Section -->

    <!-- <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; <?//= Yii::t('app', Yii::$app->name) ?> <?//= date('Y') ?></p>
        <p class="pull-right"><?//= Yii::powered() ?></p>
        </div>
    </footer> -->

    <?php $this->endBody() ?>

    <?php
      $deadline = date_create("2016-07-16"); //deadline sendind hw
      $time_teacher = " 08:00:00"; //teacher time_teacher

      $now = date_create("2016-07-26");//exacately time of sending
      $time_student = "08:00:00"; //actually tie of sending work of studdent

      if (($time_student <= $time_teacher) && ($now) <= ($deadline)) {
        echo "You can send your project";
      }else {
        echo "Sorry! your time has been finished";
      }
     ?>
</body>
</html>
<?php $this->endPage() ?>

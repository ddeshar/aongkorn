<?php

/* @var $this yii\web\View */
$this->title = Yii::t('app', Yii::$app->name);
?>
    <!-- Start Home Page Slider -->
    <section id="home">
      <!-- Carousel -->
      <div id="main-slide" class="carousel slide" data-ride="carousel">

        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#main-slide" data-slide-to="0" class="active"></li>
          <li data-target="#main-slide" data-slide-to="1"></li>
          <li data-target="#main-slide" data-slide-to="2"></li>
        </ol>
        <!--/ Indicators end-->

        <!-- Carousel inner -->
        <div class="carousel-inner">
          <div class="item active">
            <img class="img-responsive" src="/themes/newa/images/slider/bg1.jpg" alt="slider">
            <div class="slider-content">
              <div class="col-md-12 text-center">
                <h2 class="animated2">
                              <span>ยินดีต้อนรับเข้าสู่ เว็บ <strong>#องค์กรบริหารนิสิต 59</strong></span>
                              </h2>
                <h3 class="animated3">
                                <span>วิทยาลัยสงฆ์พุทธโสธร มหาวิทยาลัยมหาจุฬาลงกรณราชวิทยาลัย</span>
                              </h3>
                <p class="animated4"><a href="#" class="slider btn btn-system btn-large">Check Now</a>
                </p>
              </div>
            </div>
          </div>
          <!--/ Carousel item end -->
          <div class="item">
            <img class="img-responsive" src="/themes/newa/images/slider/bg2.jpg" alt="slider">
            <div class="slider-content">
              <div class="col-md-12 text-center">
                <h2 class="animated4">
                                <span><strong>Margo</strong> for the highest</span>
                            </h2>
                <h3 class="animated5">
                              <span>The Key of your Success</span>
                            </h3>
                <p class="animated6"><a href="#" class="slider btn btn-system btn-large">Buy Now</a>
                </p>
              </div>
            </div>
          </div>
          <!--/ Carousel item end -->
          <div class="item">
            <img class="img-responsive" src="/themes/newa/images/slider/bg3.jpg" alt="slider">
            <div class="slider-content">
              <div class="col-md-12 text-center">
                <h2 class="animated7 white">
                                <span>The way of <strong>Success</strong></span>
                            </h2>
                <h3 class="animated8 white">
                              <span>Why you are waiting</span>
                            </h3>
                <div class="">
                  <a class="animated4 slider btn btn-system btn-large btn-min-block" href="#">Get Now</a><a class="animated4 slider btn btn-default btn-min-block" href="#">Live Demo</a>
                </div>
              </div>
            </div>
          </div>
          <!--/ Carousel item end -->
        </div>
        <!-- Carousel inner end-->

        <!-- Controls -->
        <a class="left carousel-control" href="#main-slide" data-slide="prev">
          <span><i class="fa fa-angle-left"></i></span>
        </a>
        <a class="right carousel-control" href="#main-slide" data-slide="next">
          <span><i class="fa fa-angle-right"></i></span>
        </a>
      </div>
      <!-- /carousel -->
    </section>
    <!-- End Home Page Slider -->

    <!-- Start Services Section -->
    <div class="section service">
      <div class="container">
        <div class="row">

          <!-- Start Service Icon 1 -->
          <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <i class="fa fa-book icon-large"></i>
            </div>
            <div class="service-content">
              <a href="../article/view?id=1">
                <h4>ฝ่ายวิชาการ</h4>
                <p>ฝ่ายนี้ตามปกติแล้วจะเป็นฝ่ายที่คอยทำหน้าที่เกี่ยวกับการจัดหากิจกรรม,หรือโครงการที่เป็นไปในทางส่งเสริมการเรียนรู้ หรือเป็นผู้หาเวทีฝึกฝนผีปากมาเป็นเวทีฝึกฝนแก่นิสิต และนอกจากนั้น หากในมหาวิทยาลัยมีการ... </p>
              </a>
            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="02">
            <div class="service-icon">
              <i class="fa fa-bullhorn icon-large"></i>
            </div>
            <div class="service-content">
              <a href="../article/view?id=1">
                <h4>ฝ่ายนิสิตสัมพันธ์</h4>
                <p>มีหน้าที่เสมือนแผนกรับเรื่องร้องทุกข์ของนิสิตในมหาวิทยาลัย อาทิ เรื่องนิสิตขาดเรียนเยอะ,เรื่องนิสิตไม่มีเงินลงทะเบียน,นิสิตมีปัญหาทางการศึกษาเล่าเรียน,นิสิตไม่มีที่พักจำพรรษาหรือที่พักเรียน เป็นต้น... </p>
              </a>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="03">
            <div class="service-icon">
              <i class="fa fa-flag-checkered  icon-large"></i>
            </div>
            <div class="service-content">
              <a href="../article/view?id=1">
                <h4>ฝ่ายกิจกรรม</h4>
                <p>ปฏิบัติหน้าที่ในการกำกับดูแลงานในส่วนขององค์กรบริหารนิสิตให้เป็นไปด้วยความเรียบร้อย ส่งเสริมและสนับสนุนการจัดกิจกรรมต่างๆ เพื่อเสริมสร้างและพัฒนาศักยภาพขององค์กรบริหารนิสิตขึ้นภายในสถานศึกษา...</p>
              </a>
            </div>
          </div>
          <!-- End Service Icon 3 -->

          <!-- Start Service Icon 4 -->
          <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="04">
            <div class="service-icon">
              <i class="fa fa-reply-all icon-large"></i>
            </div>
            <div class="service-content">
              <a href="../article/view?id=1">
                <h4>ฝ่ายปฏิคม</h4>
                <p>ฝ่ายนี้มีหน้าที่เกี่ยวกับการจัดสถานที่ในงานต่างๆทุกงาน ไม่ว่าจะเป็นประชุมสามัญ, วิสามัญหรืองานกิจกรรมเสริมหลักสูตร เป็นต้น นอกจากนี้ยังมีหน้าที่ถวายการต้อนรับพระมหาเถรานุเถระและฆราวาสผู้ใหม่ที่มาเยี่ยมเยือน...</p>
              </a>
            </div>
          </div>
          <!-- End Service Icon 4 -->

          <!-- Start Service Icon 5 -->
          <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="05">
            <div class="service-icon">
              <i class="fa fa-btc icon-large"></i>
            </div>
            <div class="service-content">
              <a href="../article/view?id=1">
                <h4>ฝ่ายการเงิน บัญชีและพัสดุภัณฑ์</h4>
                <p>มีหน้าที่ทำรายงานบัญชีเกี่ยวกับรายรับรายจ่ายภายในองค์กรและแจ้งยอดเงินให้ที่ประชุมทราบทุกครั้งที่มีการประชุมสามัญหรือมีการขอตรวจสอบ นอกจากนี้ยังมีหน้าที่สั่งซื้ออุปกรณ์ที่ทางคณะกรรมการลงมติเห็นสมควรว่า จำเป็นต้องมีด้วย...</p>
              </a>
            </div>
          </div>
          <!-- End Service Icon 5 -->

          <!-- Start Service Icon 6 -->
          <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="06">
            <div class="service-icon">
              <i class="fa fa-coffee icon-large"></i>
            </div>
            <div class="service-content">
              <a href="../article/view?id=1">
                <h4>ฝ่ายสวัสดิการ</h4>
                <p>สวัสดิการ มีหน้าที่บริการกิจกรรมใดๆ ที่หน่วยงานองค์กรจัดให้มีขึ้นเพื่อให้ผู้ที่ปฏิบัติงานอยู่ในองค์กรนั้นๆ ได้รับความสะดวกสบายในการทำงาน มีความมั่นคง มีหลักประกันที่แน่นอนในการดำเนินชีวิตหรือได้รับประโยชน์อื่นใดนอกเหนือจากเงินเดือน</p>
              </a>
            </div>
          </div>
          <!-- End Service Icon 6 -->

          <!-- Start Service Icon 7 -->
          <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="07">
            <div class="service-icon">
              <i class="fa fa-paper-plane icon-large"></i>
            </div>
            <div class="service-content">
              <a href="../article/view?id=1">
                <h4>ฝ่ายประชาสัมพันธ์</h4>
                <p>มีหน้าที่แจ้งเรื่องภาระงานต่างๆทั้งหมดที่นิสิตในมหาวิทยาลัยต้องทราบและควรทราบให้รับรู้ อาทิ กิจกรรมเสริมหลักสูตร, การปฏิบัติธรรมประจำปี เป็นต้น นอกจากนี้ ยังมีหน้าที่ในการทำกระดานบอร์ดข่าวสารไว้แจ้งเรื่องราวต่างๆที่เกิดขึ้นภายใน...</p>
              </a>
            </div>
          </div>
          <!-- End Service Icon 7 -->

          <!-- Start Service Icon 8 -->
          <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="08">
            <div class="service-icon">
              <i class="fa fa-pencil-square-o  icon-large"></i>
            </div>
            <div class="service-content">
              <a href="../article/view?id=1">
                <h4>ฝ่ายทะเบียน</h4>
                <p>มีหน้าที่ดูแลเอกสารขององค์กรนิสิต ไม่ว่าจะเป็นเอกสารการเข้าประชุม เอกสารโครงการ เอกสารการเซ็นชื่อเข้าปฏิบัติงานของสมาชิกองค์กรนิสิต และหลักฐานการลงทะเบียนของนิสิตในหารเข้าร่วมโครองการและกิจกรรมต่างๆที่องค์กรนิสิตเป็นผู้จัด</p>
              </a>
            </div>
          </div>
          <!-- End Service Icon 8 -->

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->

    <!-- Carousel -->
    <div id="main-slide" class="carousel slide">

      <!-- Carousel inner -->
      <div class="carousel-inner">
        <div class="item active">
          <img class="img-responsive" src="/themes/newa/images/newa.jpg" alt="slider">
        </div>
        <!--/ Carousel item end -->
      </div>
      <!-- Carousel inner end-->

    </div>
    <!-- /carousel -->

    <!-- Start Portfolio Section -->
    <div class="section full-width-portfolio" style="border-top:0; border-bottom:0; background:#fff;">

      <!-- Start Big Heading -->
      <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
        <h1>กิจกรรม <strong>องค์กรนิสิต</strong></h1>
      </div>
      <!-- End Big Heading -->

      <!-- <p class="text-center">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
        <br/>veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p> -->


      <!-- Start Recent Projects Carousel -->
      <ul id="portfolio-list" data-animated="fadeIn">
        <li>
          <img src="/themes/newa/images/portfolio-1/1.png" alt="" />
          <div class="portfolio-item-content">
            <span class="header">วันไหว้ครู</span>
            <p class="body">ทดสอบไหว้ครูนะครับผม</p>
          </div>
          <a href="../article/view?id=1"><i class="more">+</i></a>

        </li>
        <li>
          <img src="/themes/newa/images/portfolio-1/2.png" alt="" />
          <div class="portfolio-item-content">
            <span class="header">ฝ่ายวิชาการ</span>
            <p class="body">ฝ่ายวิชาการฝ่ายวิชาการฝ่ายวิชาการ </p>
          </div>
          <a href="../article/view?id=1"><i class="more">+</i></a>

        </li>
        <li>
          <img src="/themes/newa/images/portfolio-1/3.png" alt="" />
          <div class="portfolio-item-content">
            <span class="header">ฝ่ายนิสิตสัมพันธ์</span>
            <p class="body">ฝ่ายนิสิตสัมพันธ์ฝ่ายนิสิตสัมพันธ์ฝ่ายนิสิตสัมพันธ์</p>
          </div>
          <a href="../article/view?id=1"><i class="more">+</i></a>

        </li>
        <li>
          <img src="/themes/newa/images/portfolio-1/4.png" alt="" />
          <div class="portfolio-item-content">
            <span class="header">ฝ่ายกิจกรรม</span>
            <p class="body">ฝ่ายกิจกรรมฝ่ายกิจกรรมฝ่ายกิจกรรมฝ่ายกิจกรรมฝ่ายกิจกรรม</p>
          </div>
          <a href="../article/view?id=1"><i class="more">+</i></a>

        </li>
        <li>
          <img src="/themes/newa/images/portfolio-1/5.png" alt="" />
          <div class="portfolio-item-content">
            <span class="header">ฝ่ายประชาสัมพันธ์และเผยแผ่ฝ่ายประชาสัมพันธ์และเผยแผ่ฝ่ายประชาสัมพันธ์และเผยแผ่</span>
            <p class="body">ฝ่ายประชาสัมพันธ์และเผยแผ่ฝ่ายประชาสัมพันธ์และเผยแผ่ฝ่ายประชาสัมพันธ์และเผยแผ่ฝ่ายประชาสัมพันธ์และเผยแผ่ฝ่ายประชาสัมพันธ์และเผยแผ่</p>
          </div>
          <a href="../article/view?id=1"><i class="more">+</i></a>

        </li>
        <li>
          <img src="/themes/newa/images/portfolio-1/6.png" alt="" />
          <div class="portfolio-item-content">
            <span class="header">เลขานุการ</span>
            <p class="body">เลขานุการ</p>
          </div>
          <a href="../article/view?id=1"><i class="more">+</i></a>

        </li>
        <li>
          <img src="/themes/newa/images/portfolio-1/7.png" alt="" />
          <div class="portfolio-item-content">
            <span class="header">ฝ่ายปฏิคม</span>
            <p class="body">ฝ่ายปฏิคม</p>
          </div>
          <a href="../article/view?id=1"><i class="more">+</i></a>

        </li>
        <li>
          <img src="/themes/newa/images/portfolio-1/8.png" alt="" />
          <div class="portfolio-item-content">
            <span class="header">ฝ่ายการเงิน</span>
            <p class="body">ฝ่ายการเงิน</p>
          </div>
          <a href="../article/view?id=1"><i class="more">+</i></a>

        </li>
      </ul>

      <!-- End Recent Projects Carousel -->


    </div>
    <!-- End Portfolio Section -->

    <!-- Start Testimonials Section -->
      <div class="container">
        <div class="row">
          <div class="col-md-8">

            <!-- Start Recent Posts Carousel -->
            <div class="latest-posts">
              <h4 class="classic-title"><span>บทความล่าสุด</span></h4>
              <div class="latest-posts-classic custom-carousel touch-carousel" data-appeared-items="2">

                <!-- Posts 1 -->
                <div class="post-row item">
                  <div class="left-meta-post">
                    <div class="post-date"><span class="day">12</span><span class="month">Dec</span></div>
                    <div class="post-type"><i class="fa fa-picture-o"></i></div>
                  </div>
                  <h3 class="post-title"><a href="#">พิธีสามีจิกรรมไหว้ครู ประจำปีการศึกษา 2559 </a></h3>
                  <div class="post-content">
                    <p>เมื่อวันที่ 12 กรกฎาคม 2559 ทาง ‪องค์กรบริหารนิสิต‬ ร่วมมือกับ กองกิจการนิสิตวิทยาลัยสงฆ์พุทโสธร ได้จัด ‪กิจกรรมไหว้ครู‬ โดยมีการตั้งขบวนแห่ เพื่อไปสักการะหลวงพ่อพุทธโสธร ณ พระอุโบสถวัดโสธรวราราม แล้ว ‪อัญเชิญหลวงพ่อโสธร‬ (จำลอง 9 นิ้ว) นำไปประดิษฐาน ณ ห้องประชุม 1 อาคารเรียนวิทยาลัย</a></p>
                  </div>
                </div>

                <!-- Posts 2 -->
                <div class="post-row item">
                  <div class="left-meta-post">
                    <div class="post-date"><span class="day">26</span><span class="month">Dec</span></div>
                    <div class="post-type"><i class="fa fa-picture-o"></i></div>
                  </div>
                  <h3 class="post-title"><a href="#">๑ ทศวรรษ ก้าวไกลไปกับวิทยาลัยสงฆ์พุทธโสธร</a></h3>
                  <div class="post-content">
                    <p>เมื่อวันอังคารที่ 12 กรกฎาคม 2559 วิทยาลัยสงฆ์พุทธโสธร จัดโครงการปฐมนิเทศนิสิต และได้สัมมนาเรื่อง "๑ ทศวรรษ ก้าวไกลไปกับวิทยาลัยสงฆ์พุทธโสธร‬" ณ ห้องประชุม 1 อาคารเรียนวิทยาลัย </a></p>
                  </div>
                </div>

                <!-- Posts 3 -->
                <div class="post-row item">
                  <div class="left-meta-post">
                    <div class="post-date"><span class="day">26</span><span class="month">Dec</span></div>
                    <div class="post-type"><i class="fa fa-picture-o"></i></div>
                  </div>
                  <h3 class="post-title"><a href="#">Iframe Video Post</a></h3>
                  <div class="post-content">
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit. <a class="read-more" href="#">Read More...</a></p>
                  </div>
                </div>

                <!-- Posts 4 -->
                <div class="post-row item">
                  <div class="left-meta-post">
                    <div class="post-date"><span class="day">26</span><span class="month">Dec</span></div>
                    <div class="post-type"><i class="fa fa-picture-o"></i></div>
                  </div>
                  <h3 class="post-title"><a href="#">Gallery Post</a></h3>
                  <div class="post-content">
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit. <a class="read-more" href="#">Read More...</a></p>
                  </div>
                </div>

                <!-- Posts 5 -->
                <div class="post-row item">
                  <div class="left-meta-post">
                    <div class="post-date"><span class="day">26</span><span class="month">Dec</span></div>
                    <div class="post-type"><i class="fa fa-picture-o"></i></div>
                  </div>
                  <h3 class="post-title"><a href="#">Standard Post without Image</a></h3>
                  <div class="post-content">
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit. <a class="read-more" href="#">Read More...</a></p>
                  </div>
                </div>

                <!-- Posts 6 -->
                <div class="post-row item">
                  <div class="left-meta-post">
                    <div class="post-date"><span class="day">26</span><span class="month">Dec</span></div>
                    <div class="post-type"><i class="fa fa-picture-o"></i></div>
                  </div>
                  <h3 class="post-title"><a href="#">Iframe Audio Post</a></h3>
                  <div class="post-content">
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit. <a class="read-more" href="#">Read More...</a></p>
                  </div>
                </div>

              </div>
            </div>
            <!-- End Recent Posts Carousel -->

          </div>

          <div class="col-md-4">

            <!-- Classic Heading -->
            <h4 class="classic-title"><span>What they say?</span></h4>

            <!-- Start Testimonials Carousel -->
            <div class="custom-carousel show-one-slide touch-carousel" data-appeared-items="1">
              <!-- Testimonial 1 -->
              <div class="classic-testimonials item">
                <div class="testimonial-content">
                  <p>เราไม่สามารถเปลี่ยนไพ่ที่เราได้รับ แต่เราจะต้องรู้ว่า จะวางแผนเล่นมันอย่างไร </br> We can’t change the cards, We’re dealt, Just how we play the hand.</p>
                </div>
                <div class="testimonial-author"><span>พระมหาดีเป็นดร้า จนฺทโสภโณ</span> - หัวหน้าฝ่ายประชาสัมพันธ์และเผยแผ่‬</div>
              </div>
              <!-- Testimonial 2 -->
              <div class="classic-testimonials item">
                <div class="testimonial-content">
                  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <div class="testimonial-author"><span>John Doe</span> - Customer</div>
              </div>
              <!-- Testimonial 3 -->
              <div class="classic-testimonials item">
                <div class="testimonial-content">
                  <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <div class="testimonial-author"><span>John Doe</span> - Customer</div>
              </div>
            </div>
            <!-- End Testimonials Carousel -->

          </div>
        </div>
      </div>
    <!-- End Testimonials Section -->

    <!-- Start Team Member Section -->
    <div class="section" style="background:#fff;">
      <div class="container">

        <!-- Start Big Heading -->
        <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
          <h1>ผู้บริหาร <strong> องค์กรบริหารนิสิต</strong></h1>
        </div>
        <!-- End Big Heading -->

        <!-- Start Team Members -->
        <div class="row">

          <!-- Start Memebr 1 -->
          <div class="col-md-3 col-sm-6 col-xs-12" data-animation="fadeIn" data-animation-delay="03">
            <div class="team-member modern">
              <!-- Memebr Photo, Name & Position -->
              <div class="member-photo">
                <img alt="" src="/themes/newa/images/team/face_1.png" />
                <div class="member-name">พระใบฎีกากฤษฎา	กตปุญฺโญ<span>ประธานสภานิสิต</span>
                </div>
              </div>
              <!-- Memebr Words -->
              <div class="member-info">
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore fugiat.</p>
              </div>
              <!-- Start Progress Bar 1 -->
              <div class="progress-label">นักธรรม</div>
              <div class="progress">
                <div class="progress-bar progress-bar-primary" data-progress-animation="96%" data-appear-animation-delay="400">
                  <span class="percentage">96%</span>
                </div>
              </div>
              <!-- Start Progress Bar 2 -->
              <div class="progress-label">พระบาลี</div>
              <div class="progress">
                <div class="progress-bar progress-bar-primary" data-progress-animation="88%" data-appear-animation-delay="800">
                  <span class="percentage">88%</span>
                </div>
              </div>
              <!-- Start Progress Bar 3 -->
              <div class="progress-label">การศึกษา</div>
              <div class="progress">
                <div class="progress-bar progress-bar-primary" data-progress-animation="100%" data-appear-animation-delay="1200">
                  <span class="percentage">100%</span>
                </div>
              </div>
              <!-- Memebr Social Links -->
              <div class="member-socail">
                <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                <a class="gplus" href="#"><i class="fa fa-google-plus"></i></a>
                <a class="mail" href="#"><i class="fa fa-envelope"></i></a>
              </div>
            </div>
          </div>
          <!-- End Memebr 1 -->

          <!-- Start Memebr 2 -->
          <div class="col-md-3 col-sm-6 col-xs-12" data-animation="fadeIn" data-animation-delay="04">
            <div class="team-member modern">
              <!-- Memebr Photo, Name & Position -->
              <div class="member-photo">
                <img alt="" src="/themes/newa/images/team/face_2.png" />
                <div class="member-name">พระมหาภาณุวัฒน์ วฑฺฒธมฺโม<span>นายกองค์กรบริหารนิสิต</span>
                </div>
              </div>
              <!-- Memebr Words -->
              <div class="member-info">
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore fugiat.</p>
              </div>
              <!-- Start Progress Bar 1 -->
              <div class="progress-label">นักธรรม</div>
              <div class="progress">
                <div class="progress-bar progress-bar-primary" data-progress-animation="96%" data-appear-animation-delay="400">
                  <span class="percentage">96%</span>
                </div>
              </div>
              <!-- Start Progress Bar 2 -->
              <div class="progress-label">พระบาลี</div>
              <div class="progress">
                <div class="progress-bar progress-bar-primary" data-progress-animation="88%" data-appear-animation-delay="800">
                  <span class="percentage">88%</span>
                </div>
              </div>
              <!-- Start Progress Bar 3 -->
              <div class="progress-label">การศึกษา</div>
              <div class="progress">
                <div class="progress-bar progress-bar-primary" data-progress-animation="100%" data-appear-animation-delay="1200">
                  <span class="percentage">100%</span>
                </div>
              </div>
              <!-- Memebr Social Links -->
              <div class="member-socail">
                <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                <a class="gplus" href="#"><i class="fa fa-google-plus"></i></a>
                <a class="mail" href="#"><i class="fa fa-envelope"></i></a>
              </div>
            </div>
          </div>
          <!-- End Memebr 2 -->

          <!-- Start Memebr 3 -->
          <div class="col-md-3 col-sm-6 col-xs-12" data-animation="fadeIn" data-animation-delay="05">
            <div class="team-member modern">
              <!-- Memebr Photo, Name & Position -->
              <div class="member-photo">
                <img alt="" src="/themes/newa/images/team/face_3.png" />
                <div class="member-name">พระปลัดชนินทร์	อภินนฺโท<span>รองนายกฯรูปที่ ๑</span>
                </div>
              </div>
              <!-- Memebr Words -->
              <div class="member-info">
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore fugiat.</p>
              </div>
              <!-- Start Progress Bar 1 -->
              <div class="progress-label">นักธรรม</div>
              <div class="progress">
                <div class="progress-bar progress-bar-primary" data-progress-animation="96%" data-appear-animation-delay="400">
                  <span class="percentage">96%</span>
                </div>
              </div>
              <!-- Start Progress Bar 2 -->
              <div class="progress-label">พระบาลี</div>
              <div class="progress">
                <div class="progress-bar progress-bar-primary" data-progress-animation="88%" data-appear-animation-delay="800">
                  <span class="percentage">88%</span>
                </div>
              </div>
              <!-- Start Progress Bar 3 -->
              <div class="progress-label">การศึกษา</div>
              <div class="progress">
                <div class="progress-bar progress-bar-primary" data-progress-animation="100%" data-appear-animation-delay="1200">
                  <span class="percentage">100%</span>
                </div>
              </div>
              <!-- Memebr Social Links -->
              <div class="member-socail">
                <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                <a class="gplus" href="#"><i class="fa fa-google-plus"></i></a>
                <a class="mail" href="#"><i class="fa fa-envelope"></i></a>
              </div>
            </div>
          </div>
          <!-- End Memebr 3 -->

          <!-- Start Memebr 4 -->
          <div class="col-md-3 col-sm-6 col-xs-12" data-animation="fadeIn" data-animation-delay="06">
            <div class="team-member modern">
              <!-- Memebr Photo, Name & Position -->
              <div class="member-photo">
                <img alt="" src="/themes/newa/images/team/face_4.png" />
                <div class="member-name">พระครูใบฎีกาทศพล ขนฺติธมฺโม<span>รองนายกฯรูปที่ ๒</span>
                </div>
              </div>
              <!-- Memebr Words -->
              <div class="member-info">
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore fugiat.</p>
              </div>
              <!-- Start Progress Bar 1 -->
              <div class="progress-label">นักธรรม</div>
              <div class="progress">
                <div class="progress-bar progress-bar-primary" data-progress-animation="96%" data-appear-animation-delay="400">
                  <span class="percentage">96%</span>
                </div>
              </div>
              <!-- Start Progress Bar 2 -->
              <div class="progress-label">พระบาลี</div>
              <div class="progress">
                <div class="progress-bar progress-bar-primary" data-progress-animation="88%" data-appear-animation-delay="800">
                  <span class="percentage">88%</span>
                </div>
              </div>
              <!-- Start Progress Bar 3 -->
              <div class="progress-label">การศึกษา</div>
              <div class="progress">
                <div class="progress-bar progress-bar-primary" data-progress-animation="100%" data-appear-animation-delay="1200">
                  <span class="percentage">100%</span>
                </div>
              </div>
              <!-- Memebr Social Links -->
              <div class="member-socail">
                <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
                <a class="gplus" href="#"><i class="fa fa-google-plus"></i></a>
                <a class="mail" href="#"><i class="fa fa-envelope"></i></a>
              </div>
            </div>
          </div>
          <!-- End Memebr 4 -->

        </div>
        <!-- End Team Members -->

      </div>
      <!-- .container -->
    </div>
    <!-- End Team Member Section -->

    <!--Start counter -->
    <div class="section full-width-portfolio" >
      <div id="parallax-one" style="background-image:url(/themes/newa/images/parallax/bg-02.jpg);">
        <div class="parallax-text-container-1">
          <div class="parallax-text-item">
            <div class="container">
              <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3">
                  <div class="counter-item">
                    <i class="fa fa-users"></i>
                    <div class="timer" id="item1" data-to="36" data-speed="5000"></div>
                    <h5>จำนวนสมาชิกองค์กร</h5>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3">
                  <div class="counter-item">
                    <i class="fa fa-users"></i>
                    <div class="timer" id="item2" data-to="18" data-speed="5000"></div>
                    <h5>จำนวนสมาชิกสภา</h5>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3">
                  <div class="counter-item">
                    <i class="fa fa-graduation-cap"></i>
                    <div class="timer" id="item3" data-to="365" data-speed="5000"></div>
                    <h5>จำนวนนิสิต</h5>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3">
                  <div class="counter-item">
                    <i class="fa fa-btc"></i>
                    <div class="timer" id="item4" data-to="400" data-speed="5000"></div>
                    <h5>ยอดเงินที่เหลือ</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    <!--End counter -->

    <!-- Start Client/support Section -->
    <div class="partner">
      <div class="container">
        <div class="row">

          <!-- Start Big Heading -->
          <div class="big-title text-center">
            <h1>ขอ<strong>ขอบคุณ</strong></h1>
            <p class="title-desc">ที่สนับสนุนเรา</p>
          </div>
          <!-- End Big Heading -->

          <!--Start Clients Carousel-->
          <div class="our-clients">
            <div class="clients-carousel custom-carousel touch-carousel navigation-3" data-appeared-items="5" data-navigation="true">

              <!-- Client 1 -->
              <div class="client-item item">
                <a href="#"><img src="/themes/newa/images/support/c1.png" alt="" /></a>
              </div>

              <!-- Client 2 -->
              <div class="client-item item">
                <a href="#"><img src="/themes/newa/images/support/c2.png" alt="" /></a>
              </div>

              <!-- Client 3 -->
              <div class="client-item item">
                <a href="#"><img src="/themes/newa/images/support/c3.png" alt="" /></a>
              </div>

              <!-- Client 3 -->
              <div class="client-item item">
                <a href="#"><img src="/themes/newa/images/support/c3.png" alt="" /></a>
              </div>

              <!-- Client 3 -->
              <div class="client-item item">
                <a href="#"><img src="/themes/newa/images/support/c3.png" alt="" /></a>
              </div>

              <!-- Client 3 -->
              <div class="client-item item">
                <a href="#"><img src="/themes/newa/images/support/c3.png" alt="" /></a>
              </div>

            </div>
          </div>
          <!-- End Clients Carousel -->
        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Client/Partner Section -->

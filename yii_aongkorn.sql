-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2016 at 04:06 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yii_aongkorn`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `summary` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `user_id`, `title`, `summary`, `content`, `status`, `category`, `created_at`, `updated_at`) VALUES
(1, 1, 'mflv[', 'asdfj', '<p>asdf</p>\r\n', 2, 1, 1466513480, 1466513480);

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('theCreator', 1, 1466512953);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, 'Administrator of this application', NULL, NULL, 1466512876, 1466512876),
('adminArticle', 2, 'Allows admin+ roles to manage articles', NULL, NULL, 1466512875, 1466512875),
('createArticle', 2, 'Allows editor+ roles to create articles', NULL, NULL, 1466512875, 1466512875),
('deleteArticle', 2, 'Allows admin+ roles to delete articles', NULL, NULL, 1466512875, 1466512875),
('editor', 1, 'Editor of this application', NULL, NULL, 1466512876, 1466512876),
('manageUsers', 2, 'Allows admin+ roles to manage users', NULL, NULL, 1466512875, 1466512875),
('member', 1, 'Registered users, members of this site', NULL, NULL, 1466512875, 1466512875),
('premium', 1, 'Premium members. They have more permissions than normal members', NULL, NULL, 1466512875, 1466512875),
('support', 1, 'Support staff', NULL, NULL, 1466512876, 1466512876),
('theCreator', 1, 'You!', NULL, NULL, 1466512876, 1466512876),
('updateArticle', 2, 'Allows editor+ roles to update articles', NULL, NULL, 1466512875, 1466512875),
('updateOwnArticle', 2, 'Update own article', 'isAuthor', NULL, 1466512875, 1466512875),
('usePremiumContent', 2, 'Allows premium+ roles to use premium content', NULL, NULL, 1466512875, 1466512875);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('theCreator', 'admin'),
('editor', 'adminArticle'),
('editor', 'createArticle'),
('admin', 'deleteArticle'),
('admin', 'editor'),
('admin', 'manageUsers'),
('support', 'member'),
('support', 'premium'),
('editor', 'support'),
('admin', 'updateArticle'),
('updateOwnArticle', 'updateArticle'),
('editor', 'updateOwnArticle'),
('premium', 'usePremiumContent');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_rule`
--

INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('isAuthor', 'O:28:"common\\rbac\\rules\\AuthorRule":3:{s:4:"name";s:8:"isAuthor";s:9:"createdAt";i:1466512875;s:9:"updatedAt";i:1466512875;}', 1466512875, 1466512875);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1466512813),
('m141022_115823_create_user_table', 1466512817),
('m141022_115912_create_rbac_tables', 1466512819),
('m141022_115922_create_session_table', 1466512819),
('m150104_153617_create_article_table', 1466512819);

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `id` char(64) COLLATE utf8_unicode_ci NOT NULL,
  `expire` int(11) NOT NULL,
  `data` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `expire`, `data`) VALUES
('02dqhk9pi182kigcjpi0cihb85', 1467452994, 0x5f5f666c6173687c613a303a7b7d),
('3pp17s1tctomm0llrptnnc1bd5', 1467452994, 0x5f5f666c6173687c613a303a7b7d),
('54f80klnsnqk8c9377ri15tpu7', 1467528404, 0x5f5f666c6173687c613a303a7b7d5f5f636170746368612f736974652f636170746368617c733a373a22676c626f7a6967223b5f5f636170746368612f736974652f63617074636861636f756e747c693a313b),
('72qnvbnr53m5u8k3i5kckl0rs2', 1467452994, 0x5f5f666c6173687c613a303a7b7d),
('7b7o2m1es0vpk3rqoivgko99l6', 1467527757, 0x5f5f666c6173687c613a303a7b7d),
('7bmsl6omfrs05frolmaanqppl3', 1467527741, 0x5f5f666c6173687c613a303a7b7d5f5f636170746368612f736974652f636170746368617c733a373a2270656f61756c7a223b5f5f636170746368612f736974652f63617074636861636f756e747c693a313b),
('7ref06o8p0gk1u3q93cj93dtq7', 1467516021, 0x5f5f666c6173687c613a303a7b7d5f5f636170746368612f736974652f636170746368617c733a373a22626c796564617a223b5f5f636170746368612f736974652f63617074636861636f756e747c693a313b),
('9m77e5ocar7h1rdq396qeuhde4', 1467525972, 0x5f5f666c6173687c613a303a7b7d5f5f636170746368612f736974652f636170746368617c733a363a227373656d7376223b5f5f636170746368612f736974652f63617074636861636f756e747c693a313b),
('bk32hccl392smat0m7jpkm4jb4', 1467511015, 0x5f5f666c6173687c613a303a7b7d),
('cd7tjq65r79pdv2evevjor9n32', 1467466161, 0x5f5f666c6173687c613a303a7b7d),
('fuogujks18hpb1dorlhrih3fh6', 1467525979, 0x5f5f666c6173687c613a303a7b7d),
('hfpc38hbafjk10tj5q3tl5o174', 1467527746, 0x5f5f666c6173687c613a303a7b7d5f5f636170746368612f736974652f636170746368617c733a363a226b616c6b7061223b5f5f636170746368612f736974652f63617074636861636f756e747c693a313b),
('j5bkh9fmn0unv6384sfkl0tr00', 1467456561, 0x5f5f666c6173687c613a303a7b7d5f5f636170746368612f736974652f636170746368617c733a373a22717574716d6e78223b5f5f636170746368612f736974652f63617074636861636f756e747c693a313b),
('kij5c70iumehq7d5ps1b0iuil3', 1467435827, 0x5f5f666c6173687c613a303a7b7d5f5f636170746368612f736974652f636170746368617c733a363a227669686f6867223b5f5f636170746368612f736974652f63617074636861636f756e747c693a313b),
('mclobvm06757fr46nvnrptthm2', 1467427597, 0x5f5f666c6173687c613a303a7b7d),
('oupg4t5omsruspp0mto66qa4o2', 1467467510, 0x5f5f666c6173687c613a303a7b7d),
('rbu0mfd7mfupk4pet6r1iul8v5', 1467525973, 0x5f5f666c6173687c613a303a7b7d5f5f636170746368612f736974652f636170746368617c733a373a2272616a6c6b6f68223b5f5f636170746368612f736974652f63617074636861636f756e747c693a313b),
('t4hneiqve1lf658btb25d32082', 1467516199, 0x5f5f666c6173687c613a303a7b7d),
('tnld9edtgn82s7v1o418nncpm0', 1467475404, 0x5f5f666c6173687c613a303a7b7d5f5f636170746368612f736974652f636170746368617c733a363a2273697a6f7376223b5f5f636170746368612f736974652f63617074636861636f756e747c693a313b),
('vj0fe8kqc7c8nsm8k7n2figba4', 1467435191, 0x5f5f666c6173687c613a303a7b7d);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_activation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `status`, `auth_key`, `password_reset_token`, `account_activation_token`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'jedeshar@gmail.com', '$2y$13$4kunBye/dnDq3CH1GpJY9.Q4RtXDAav90CAsM9BsUaFVYkOrJXXja', 10, 'iyUKPBgcG92LCXSXst4AGcB7jSOO40bf', NULL, NULL, 1466512952, 1466512952);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
